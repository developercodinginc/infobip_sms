<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    if(isset($_POST['phone']) && !empty($_POST['phone'])){
        $phone = $_POST['phone'];
    }else {
        $phone = '';
    }
    $arrayName = array(
        'name' => $name,
        'phone' => $phone,
    );
    $_POST['name'] = $name;
    $_POST['phone'] = $phone;

    if(isset($_POST['sms']) && !empty($_POST['sms'])){
        $content = $_POST['sms'];
    }else{
        $content = '';
        exit;
    }

    // ------------------------------------------------------------------ //
    // Get the info and send SMS.

    $curl = curl_init();
    $basic = base64_encode('USERNAME:PASSWORD');
    echo $basic;
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://4rr9p.api.infobip.com/sms/2/text/advanced",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS =>  '{
                          "messages": [
                            {
                              "from": "sms",
                              "destinations": [
                                {
                                  "to": "'.$phone.'"
                                }
                              ],
                              "text": "'.$content.'",
                              "flash": false,
                              "language": {
                                "languageCode": "PT"
                              },
                              "transliteration": "Portuguese_Brazil",
                              "intermediateReport": true,
                              "notifyUrl": "https://webhook.site/c5f8947b-8a24-4654-8b49-9d2a711428bf",
                              "notifyContentType": "application/json",
                              "callbackData": "DLR callback data",
                              "validityPeriod": 720
                            }
                          ],
                          "bulkId": "BULK-ID-123-xyz",
                          "tracking": {
                            "track": "SMS",
                            "type": "MY_CAMPAIGN"
                          }
                        }',
      CURLOPT_HTTPHEADER => array(
        "accept: application/json",
        "authorization: App 9fc12d079843a1c56ad9983dbfef06ed-cc6ceecb-eabc-4046-add2-df98d97edddc",
        "content-type: application/json"
      ),
    ));

    $response = curl_exec($curl);
    $response = json_decode($response, true);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
        echo '<pre>';
        print_r($response);
        echo '</pre>';
    }
